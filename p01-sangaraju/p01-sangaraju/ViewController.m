//
//  ViewController.m
//  p01-sangaraju
//
//  Created by Rameshkumarraju Sangaraju on 2/3/16.
//  Copyright © 2016 Rameshkumarraju Sangaraju. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)firstButtonClicked:(id)sender {
    self.view.backgroundColor = [UIColor greenColor];
    _hwLabel.text = @"This is Ramesh Sangaraju";
    _firstButton.hidden = YES;
    
    
}
@end
