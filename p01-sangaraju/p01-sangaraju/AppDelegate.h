//
//  AppDelegate.h
//  p01-sangaraju
//
//  Created by Rameshkumarraju Sangaraju on 2/3/16.
//  Copyright © 2016 Rameshkumarraju Sangaraju. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

